from django.db import models

class DogModel(models.Model):
    d_name = models.CharField(max_length=30)
    d_age = models.IntegerField()
    d_breed = models.CharField(max_length=30)
    d_owner =models.CharField(max_length=30)

    def __str__(self):
    	return self.d_name