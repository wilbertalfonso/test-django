from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import View
from .models import DogModel
from .forms import DogForm
from django.shortcuts import redirect

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

class DogView(View):
    template_name = "dog/list.html"

    def get(self, request):
        context = {}
        context['dogs']= DogModel.objects.all()
        return render(request, self.template_name, context)



class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)

def dog_add(request):
    template_name = "dog/add.html"
    if request.method == "POST":
        form = DogForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('dog_list')
    else: 
        form = DogForm()
        return render(request, template_name, {'form':form})
