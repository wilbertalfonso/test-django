from django import forms
from .models import DogModel

class DogForm(forms.ModelForm):
	class Meta:
		model = DogModel
		fields = '__all__'