from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    DogView,
)
from . import views


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/list', DogView.as_view(), name="dog_list"),
    path('dog/add', views.dog_add, name="dog_add"),
]
